//
//  AboutViewController.swift
//  Bullseye
//
//  Created by Robert Marković on 08.07.2022..
//
//  CONNECTING VIEWCONTROLLER WITH STORYBOARD - in storyboard select view controller (not View!), in Identity Inspector "Class" choose right ViewController.

import UIKit
import WebKit

class AboutViewController: UIViewController {
    @IBOutlet weak var webView: WKWebView!
    
     override func viewDidLoad() {
        super.viewDidLoad()
         
         if let url = Bundle.main.url(forResource: "AboutBullseye", withExtension: ".html") {
             let request = URLRequest(url: url)
             webView.load(request)
         }

        // Do any additional setup after loading the view.
    }
    
    @IBAction func closeButton() {
        dismiss(animated: true)
    }
}

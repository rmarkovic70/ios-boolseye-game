//
//  ViewController.swift
//  Bullseye
//
//  Created by Robert Marković on 06.07.2022..
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var targetValueLabel: UILabel!
    @IBOutlet weak var scoreValueLabel: UILabel!
    //@IBOutlet weak var sliderValueLabel: UILabel!
    @IBOutlet weak var roundValueLabel: UILabel!
    
    var currentSliderValue = 50
    var targetValue = 0
    var score = 0
    var round = 1
    var difference = 0
    var points = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.startNewRound() //receiver.methodName(parameters); receiver = ViewController (this one)
        
        let backgroundImage = UIImageView(frame:UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "red_bull")
        backgroundImage.contentMode = .scaleAspectFit
        view.insertSubview(backgroundImage, at: 0)
    }
    
    func startNewRound() -> Void {
        instantiateValues()
    }
    
    func instantiateValues() {
        targetValue = Int.random(in: 1...100)
        slider.value = 50
        round = 1
        score = 0
        updateLabels()
    }

    func updateLabels() {
        targetValueLabel.text = String(targetValue) //or "\(targetValue)
        //sliderValueLabel.text = String(Int(slider.value))
        scoreValueLabel.text = String(score)
        roundValueLabel.text = String(round)
    }
    
    @IBAction func sliderMoved(_ slider: UISlider) {
        currentSliderValue = lroundf(slider.value) //zaokruzuje na cijeli broj
        //sliderValueLabel.text = String(Int(slider.value))
    }
    
    @IBAction func HitMeButton(){
        difference = abs(targetValue - currentSliderValue)
        points = 100 - difference
        //print("difference: \(difference)" + ", points: \(points)")
        score += points
        round+=1
        targetValue = Int.random(in: 1...100)
        showAlert()
        
    }
    
    func showAlert() {
        let title: String
        if difference == 0 {
            title = "Perfect!"
        } else if difference < 5 {
            title = "You almost had it!"
        } else if difference < 10 {
            title = "Pretty good!"
        } else {
            title = "Looser, not even close..."
        }
        
        let message = "You scored \(points) points."
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: { _ in //CLOUSURE; Callback pattern
            self.updateLabels()
        })
        
        alert.addAction(action)
        present(alert, animated: true)
    }
    
    @IBAction func StartOverButton(_ sender: UIButton) {
        addHighScore(score)
        startNewRound()
    }
    
    func addHighScore (_ score: Int) {
        guard score > 0
        else {
            return;
        }
        
        let highscore = HighScoreItem()
        highscore.score = score
        highscore.name = "Unknown"
        
        var highScores = PersistencyHelper.loadHighScores()
        highScores.append(highscore)
        highScores.sort { $0.score > $1.score }
        PersistencyHelper.saveHighScores(highScores)
    }
}

//
//  HighScoreItem.swift
//  Bullseye
//
//  Created by Robert Marković on 11.07.2022..
//

import Foundation

class HighScoreItem: NSObject, Codable {
    var name = ""
    var score = 0
}
